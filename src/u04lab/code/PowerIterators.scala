package u04lab.code

import Optionals._
import Lists._
import Streams._

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]):PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

    object PowerIterator{
      def apply[A](stream: Stream[A]): PowerIterator[A] =  PowerIteratorImpl(stream)
    }

    private case class PowerIteratorImpl[A](var stream:Stream[A]) extends PowerIterator[A] {
        private var pastList: List[A] = List.nil

        override def next():Option[A] = stream match{
          case Stream.Cons(h,t)=>{
            pastList = List.append(pastList,List.Cons(h(),List.nil))
            stream = t()
            Option.Some(h())
          }
          case _ => Option.empty[A]
        }
        override def allSoFar():List[A]={
          pastList
        }

        override def reversed(): PowerIterator[A] = {
          PowerIterator(List.toStream(List.reverse(pastList)))
        }
    }


  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = PowerIterator(Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] = PowerIterator(List.toStream(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = PowerIterator(Stream.take(Stream.generate(Random.nextBoolean()))(size))
}
